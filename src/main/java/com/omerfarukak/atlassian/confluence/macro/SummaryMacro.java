package com.omerfarukak.atlassian.confluence.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.upm.license.storage.lib.ThirdPartyPluginLicenseStorageManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.omerfarukak.atlassian.Utils;
import com.textteaser.summarizer.Textteaser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SummaryMacro implements Macro {

    private static final String TEMPLATE = "templates/summary-macro.vm";
    private static ThirdPartyPluginLicenseStorageManager licenseManager;

    public SummaryMacro(ThirdPartyPluginLicenseStorageManager licenseManager) {
        super();
        SummaryMacro.licenseManager = licenseManager;
    }

    @Override
    public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext)
            throws MacroExecutionException {

        Map contextMap = new HashMap<Object, Object>();
        List<String> sentenceList = new ArrayList<String>();

        if (Utils.isLicenseValid(licenseManager)) {
            String text = conversionContext.getEntity().getBodyAsStringWithoutMarkup();
            String title = conversionContext.getEntity().getTitle();

            String summary = Textteaser.summarize(title, text);

            JsonParser jParser = new JsonParser();
            JsonElement summaryJson = jParser.parse(summary);

            JsonArray sentences = summaryJson.getAsJsonObject().getAsJsonArray("sentences");

            for (JsonElement sentence : sentences) {
                String sentenceString = sentence.getAsString();

                if (sentenceString.split(" ").length > 4) {
                    sentenceList.add(sentenceString);
                }
            }
        } else {
            sentenceList.add("You need to get a license from marketplace.");
        }
        contextMap.put("sentenceList", sentenceList);

        return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
